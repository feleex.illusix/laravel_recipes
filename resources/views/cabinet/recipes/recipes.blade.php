@extends('layouts.layout')

@section('content')
<div class="container">
    <a href="/cabinet/recipes">
        <button class="btn btn-info">НАЗАД</button>
    </a>
    <hr>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <h5> {{ $error }} </h5>
            @endforeach
        </div>
    @endif

    @if (isset($id))
        {!! Form::open(['url' => '/cabinet/recipes/id/'.$id, 'method' => 'post']) !!}
    @else
        {!! Form::open(['url' => '/cabinet/addrecipes/', 'method' => 'post']) !!}
    @endif

    {{ csrf_field() }}
    <h4>Добавление рецепта</h4>
    <p>Название*</p>
    {!! Form::text('name', @$recipeName, ['class' => 'form-control', 'style' => 'width: 50%']) !!}
    <p>Описание*</p>
    {!! Form::textarea('description', @$description, ['class' => 'form-control', 'style' => 'width: 80%; height: 15vw']) !!}
    <hr>
    <div id="ingredients">
        <div class="row">
            <div class="col-lg-6">Ингредиент</div>
            <div class="col-lg-6">Количество</div>
        </div>
        @if(!empty($ingredientsList))
            @foreach ($ingredientsList as $key => $value)
                <div class="row">
                    <div class="col-lg-6">
                        {!! Form::select('ingredients['.$key.']', $ingList, $value['id'], ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-lg-3">
                        {!! Form::text('quantity['.$key.']', $quantityIng[$key]['quantity'], ['class' => 'form-control']) !!}
                    </div>
                    {!! Form::select('dimension['.$key.']', $listDimension, $recipeDimension[$key]['id'], ['class' => 'form-control', 'style' => 'float: left; width: 5vw; height: 38px']) !!}
                    <div onclick="deleteBlock(this)"><i class="fa fa-times fa-margin"></i></div>
                </div>
            @endforeach
        @endif
    </div><br>
    <div class="btn btn-info" id="add" style="float: left">Добавить</div>
    <div class="not-in-list">Нет в списке?
        <div class="btn btn-info" id="new_ing">Добавить новый ингредиент</div>
    </div>
    <hr>
    <div align="right">{!! Form::submit('Сохранить рецепт', ['class' => 'btn btn-primary']) !!}</div>
    {!! Form::close() !!}
    <div id="add_ing">
        {!! Form::open(['url' => 'cabinet/add_ingredients', 'method' => 'post']) !!}
        {{ csrf_field() }}
        <h4>Добавление ингредиента</h4><br>
        <b>Название</b> &nbsp {!! Form::text('name', old('name'), ['class' => 'form-control', 'style' => 'width: 80%']) !!} <br>
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}   
    </div>
</div>
<script src="{{ asset('js/recipes.js') }}"></script>
@endsection
