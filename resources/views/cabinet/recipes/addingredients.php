<div class="row">
    <div class="col-lg-6">
        <select class="form-control form-control-ingredients" name="ingredients[]"><?php foreach ($ingList as $key => $value) { ?>
                <option value="<?php echo $key ?>"><?php echo $value ?></option><?php } ?>
        </select>
    </div>
    <div class="col-lg-3">
        <input class="form-control" type="text" name="quantity[]" value="">
    </div>
    <select class="form-control form-control-dimension" name="dimension[]"><?php foreach ($listDimension as $key => $val) { ?>
            <option value="<?php echo $key ?>"><?php echo $val ?></option><?php } ?>
    </select>
    <div href="#" onclick="deleteBlock(this); return false"><i class="fa fa-times fa-margin"></i></div>
</div>