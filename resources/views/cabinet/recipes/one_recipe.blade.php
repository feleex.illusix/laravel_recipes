@extends('layouts.layout')

@section('content')
<div class="container">
	<a href="/cabinet/recipes"><button class="btn btn-info">НАЗАД</button></a>
	<hr>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<h5> {{ $error }} </h5>
			@endforeach
		</div>
	@endif

	<h3>{{ $oneRecipe->name }}</h3><br>
	<h5>{{ $oneRecipe->description }}</h5>

	<hr><br><h4>Ингредиенты</h4><br>
	{!! Form::open(['url' => 'cabinet/recipe/'.$id, 'method' => 'post']) !!}
		{{ csrf_field() }}
	{{ Form::hidden('name','true') }}
		@foreach ($ingList as $key => $value)
			<div class="name-width">{{ $value['name'] }}</div>
			<div class="ing-align">
				{!! Form::text('quantity['.$key.']', $quantity[$key]['quantity'], ['class' => 'form-control']) !!}
			</div>
			{!! Form::select('dimension['.$key.']', $listDimension, $recipeDimension[$key]['id'], ['class' => 'form-control', 'style' => 'float: left; width: 5vw; height: 38px']) !!}
			<div style="clear: both;"></div>
			<hr>
		@endforeach
	{!! Form::submit('Сохранить ингредиент', ['class' => 'btn btn-primary', 'style' => 'display: none;']) !!}
	{!! Form::close() !!}

</div>
@endsection
