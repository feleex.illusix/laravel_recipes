@extends('layouts.layout')

@section('content')
<div class="container">
	<a href="/cabinet/"><button class="btn btn-info">НАЗАД</button></a>
	<a href="/cabinet/addrecipes"><button class="btn btn-default float-right">Добавить рецепт</button></a><hr>

	@if (session('status'))
		<div class="alert alert-success">
			<h4> {{ session('status') }} </h4>
		</div>
	@endif

	<h3>Рецепты</h3><br>
	<div class="row">
		<div class="col-12 col-sm-6 col-md-3">
			<div class="info-box info-box-color">
				<b>Рецепт</b>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="card card-color">
				<div class="card-body" style="padding: .5rem;">
					<b>Описание</b>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-3">
			<div class="info-box info-box-color">
				<b>Действия</b>
			</div>
		</div>
	</div>
	@if (empty($allRecipes))
		<h4>У вас еще нет рецептов..</h4>
	@else
		@foreach ($allRecipes as $value)
			<div class="row">
				<div class="col-12 col-sm-6 col-md-3">
					<div class="info-box info-box-height">
						{{ $value->name }}
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-padding">
						{{ $value->description }}
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-3">
					<div class="info-box info-box-padding">
						<a href="/cabinet/recipe/{{ $value->id }}"><i class="nav-icon fa fa-file">&nbsp</i></a>
						<a href="/cabinet/recipes/id/{{ $value->id }}"><i class="nav-icon fa fa-edit">&nbsp</i></a>
						<a href="/cabinet/recipe/del/{{ $value->id }}"><i class="fa fa-times">&nbsp</i></a>
					</div>
				</div>&nbsp;
			</div>
		@endforeach
	@endif
	<hr>
	@include('layouts.pagination', ['paginator' => $allRecipes])
</div>
@endsection
