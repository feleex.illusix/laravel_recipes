@extends('layouts.layout')

@section('content')
    <div class="container">

        <a href="/cabinet/">
            <button class="btn btn-info">НАЗАД</button>
        </a>
        <a href="/cabinet/addingredient">
            <button class="btn btn-default float-right">Добавить ингредиент</button>
        </a>
        <hr>

        @if (session('status'))
            <div class="alert alert-success">
                <h4> {{ session('status') }} </h4>
            </div>
        @endif

        <h3>Ингредиенты</h3><br>
        <div class="row">
            <div class="col-lg-7">
                <div class="card card-ing-color">
                    <div class="card-body">
                        <b>Меню</b>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="card card-ing-color">
                    <div class="card-body">
                        <b>Действие</b>
                    </div>
                </div>
            </div>

            @if (empty($allIngredients))
                <h4>У вас еще нет ингредиентов..</h4>
            @else
                @foreach ($allIngredients as $value)
                    <div class="col-lg-7">
                        <div class="card">
                            <div class="card-body">
                                <b>{{ $value->name }}</b>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="card" style="text-align: center;">
                            <div class="card-body">
                                <a href="/cabinet/ingredient/id/{{ $value->id }}"><i
                                            class="nav-icon fa fa-edit"></i></a>
                                <a href="/cabinet/ingredients/del/{{ $value->id }}"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <hr>
        @include('layouts.pagination', ['paginator' => $allIngredients])
    </div>
@endsection
