@extends('layouts.layout')

@section('content')
<div class="container">
	<a href="/cabinet/ingredients"><button class="btn btn-info">НАЗАД</button></a>
	<hr>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<h5> {{ $error }} </h5>
			@endforeach
		</div>
	@endif

	@if (isset($id))
		{!! Form::open(['url' => 'cabinet/ingredient/id/'.$id, 'method' => 'post']) !!}
	@else
		{!! Form::open(['url' => 'cabinet/addingredient', 'method' => 'post']) !!}
	@endif
	{{ csrf_field() }}
	<h4>Добавление ингредиента</h4>

	Название*
	<p>
		{!! Form::text('name', $ingredientsName, ['class' => 'form-control', 'style' => 'width: 50%']) !!}
	</p>
	<hr><br>

	<div align="right">
		{!! Form::submit('Сохранить ингредиент', ['class' => 'btn btn-primary']) !!}
	</div>
	{!! Form::close() !!}
</div>
@endsection
