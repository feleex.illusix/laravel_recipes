<!DOCTYPE html>
<html lang="en">
<head> 
    <title>{{ @$title }}</title>
    <meta name="description" content="" />
     
    @include('layouts.styles')
 
</head>
<body>
     
    <div id="wrapper">
        @include('layouts/header')
 
        @yield('content')
 
        @include('layouts/footer')
    </div>
     
     
</body>
</html>