<?php
if (!empty($paginator)) {
    $linkCount = 3;
    $pageCount = $paginator->lastPage();
    $currentPage = $paginator->currentPage();

    if ($pageCount > 1) {
        $pagesEitherWay = floor($linkCount / 2);
        $paginationHtml = '<ul class="pagination">';

        $previousDisabled = $currentPage == 1 ? 'disabled' : '';
        $paginationHtml .= '<li class="page-item ' . $previousDisabled . '">
        <a class="page-link" href="/cabinet/' . $url . '/' . ($currentPage - 1) . '" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>
        </a>
        </li>';

        $startPage = ($currentPage - $pagesEitherWay) < 1 ? 1 : $currentPage - $pagesEitherWay;
        $endPage = ($currentPage + $pagesEitherWay) > $pageCount ? $pageCount : ($currentPage + $pagesEitherWay);

        if ($startPage > $pageCount - $linkCount) {
            $startPage = ($pageCount - $linkCount) + 1;
            $endPage = $pageCount;
        }

        if ($endPage <= $linkCount) {
            $startPage = 1;
            $endPage = $linkCount < $pageCount ? $linkCount : $pageCount;
        }

        for ($i = $startPage; $i <= $endPage; $i++) {
            $disabledClass = $i == $currentPage ? 'disabled' : '';
            $paginationHtml .= '<li class="page-item ' . $disabledClass . '">
            <a class="page-link" href="/cabinet/' . $url . '/' . $i . '">' . $i . '</a>
            </li>';
        }

        $nextDisabled = $currentPage == $pageCount ? 'disabled' : '';
        $paginationHtml .= '<li class="page-item ' . $nextDisabled . '">
        <a class="page-link" href="/cabinet/' . $url . '/' . ($currentPage + 1) . '" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
        </a>
        </li>';

        $paginationHtml .= '</ul>';

        echo $paginationHtml;
    }
    if ($currentPage > $pageCount) {
        exit(header('Location: /error404/'));
    }
}
?>