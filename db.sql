-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 20 2018 г., 10:10
-- Версия сервера: 5.6.38
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `add_ingridient_controllers`
--

CREATE TABLE `add_ingridient_controllers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cabinets`
--

CREATE TABLE `cabinets` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `dimension`
--

CREATE TABLE `dimension` (
  `id` int(11) NOT NULL,
  `name` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dimension`
--

INSERT INTO `dimension` (`id`, `name`) VALUES
(1, 'кг'),
(2, 'г'),
(3, 'л'),
(4, 'мл'),
(5, 'ч.л'),
(6, 'ст.л'),
(7, 'шт');

-- --------------------------------------------------------

--
-- Структура таблицы `homes`
--

CREATE TABLE `homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ingredients_to_recipes`
--

CREATE TABLE `ingredients_to_recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `dimension` int(11) NOT NULL,
  `recipe_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredients_to_recipes`
--

INSERT INTO `ingredients_to_recipes` (`id`, `ingredient_id`, `quantity`, `dimension`, `recipe_id`, `created_at`, `updated_at`) VALUES
(3, 2, 400, 4, 2, NULL, NULL),
(4, 3, 1, 3, 2, NULL, NULL),
(5, 15, 250, 2, 2, NULL, NULL),
(8, 2, 41, 3, 1, NULL, NULL),
(9, 4, 21, 1, 1, NULL, NULL),
(10, 1, 100, 4, 1, NULL, NULL),
(11, 1, 50, 5, 4, NULL, NULL),
(12, 2, 215, 4, 4, NULL, NULL),
(13, 3, 1, 3, 4, NULL, NULL),
(14, 4, 3, 1, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `ingridients`
--

CREATE TABLE `ingridients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingridients`
--

INSERT INTO `ingridients` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Oil', NULL, '2018-06-14 06:25:28'),
(2, 'Water', NULL, NULL),
(3, 'Milk', NULL, NULL),
(4, 'Tomates', NULL, '2018-06-11 06:15:07'),
(15, 'чапст', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2018_06_05_062014_create_homes_table', 1),
('2018_06_05_062251_create_cabinets_table', 1),
('2018_06_05_064106_create_add_ingridient_controllers_table', 1),
('2018_06_05_073944_create_ingridients_table', 1),
('2018_06_06_061617_create_recipes_controllers_table', 1),
('2018_06_08_124637_refresh', 1),
('2018_06_08_124751_ingridients', 1),
('2018_06_08_132020_ingridients_table', 1),
('2018_06_08_132427_create_ingridients', 1),
('2018_06_19_182757_create_recipes_table', 2),
('2018_06_20_160118_create_ingredients_to_recipes_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `recipes`
--

CREATE TABLE `recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `description`, `user`, `created_at`, `updated_at`) VALUES
(1, 'авписоьсрп ьспт прмотпр', 'сап тспрьтаплоа аенпакр мрп', 1, '2018-06-20 03:16:40', '2018-06-20 03:16:40'),
(2, 'ывматпсрьа вар  вчапсортпоь', 'саптсрь нсапьмб олбппаова пваптвер агнл пглаарс', 1, '2018-06-20 03:17:40', '2018-06-20 03:17:40'),
(4, 'ваиачсоьосрпьмь счсапт', 'чспаст сатпромрлммон ссчонпонегв', 1, '2018-06-20 03:51:07', '2018-06-20 03:51:07');

-- --------------------------------------------------------

--
-- Структура таблицы `recipes_controllers`
--

CREATE TABLE `recipes_controllers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@mail.ru', '$2y$10$KNe6gORhz53HMJ/J0EoTkeswLeDwxC2Wl6NFwv54e2LYZ/Tyh3xTK', 'y8heVdmzTeozn0B25XaqiPwu0KOJ9J3gkdKpt4MvYnVLJR2goFsfs0MNIABS', '2018-06-08 10:39:20', '2018-06-14 04:02:46');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `add_ingridient_controllers`
--
ALTER TABLE `add_ingridient_controllers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cabinets`
--
ALTER TABLE `cabinets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `dimension`
--
ALTER TABLE `dimension`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ingredients_to_recipes`
--
ALTER TABLE `ingredients_to_recipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ingredients_to_recipes_recipe_id_foreign` (`recipe_id`);

--
-- Индексы таблицы `ingridients`
--
ALTER TABLE `ingridients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `recipes_controllers`
--
ALTER TABLE `recipes_controllers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `add_ingridient_controllers`
--
ALTER TABLE `add_ingridient_controllers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cabinets`
--
ALTER TABLE `cabinets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dimension`
--
ALTER TABLE `dimension`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `homes`
--
ALTER TABLE `homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ingredients_to_recipes`
--
ALTER TABLE `ingredients_to_recipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `ingridients`
--
ALTER TABLE `ingridients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `recipes_controllers`
--
ALTER TABLE `recipes_controllers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ingredients_to_recipes`
--
ALTER TABLE `ingredients_to_recipes`
  ADD CONSTRAINT `ingredients_to_recipes_recipe_id_foreign` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
