<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsToRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients_to_recipes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('ingredient_id');
            $table->integer('quantity');
            $table->integer('dimension');
            $table->integer('recipe_id')->unsigned();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::table('ingredients_to_recipes', function($table) {
        $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredients_to_recipes');
    }
}
