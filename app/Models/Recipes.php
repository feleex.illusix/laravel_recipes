<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipes extends Model
{
    protected $fillable = ['name', 'description', 'user'];

    public function getRecipeIngredients()
    {
        return $this->belongsToMany('App\Models\Ingredients', 'ingredients_to_recipes', 'recipe_id', 'ingredient_id');
    }

    public function getDimension()
    {
        return $this->belongsToMany('App\Models\Dimension', 'ingredients_to_recipes', 'recipe_id', 'dimension');
    }

    public function insertIngerientsMass($id, $request)
    {
        foreach ($request->quantity as $key => $value) {
            $ingredients_mass[] = array(
                'ingredient_id'  => $request->ingredients[$key],
                'quantity' => $value,
                'dimension' => $request->dimension[$key],
                'recipe_id'   => $id
            );
        }
        $IngredientsToRecipes = new IngredientsToRecipes;
        $IngredientsToRecipes->insert($ingredients_mass);
    }

    public function setRecipes($request)
    {
        $lastId = $this->create($request->all())->id;
        if ($lastId) {
            $this->insertIngerientsMass($lastId, $request);
        }
    }

    public function updateRecipes($id, $request)
    {
        $recipeSave = $this->findOrFail($id)->fill($request->all())->save();
        if ($recipeSave) {
            $this->insertIngerientsMass($id, $request);
        }
    }

    public static function setQuantity($id, $request)
    {
        $ing_ids = IngredientsToRecipes::where('recipe_id', '=', $id)->get()->toArray();
        foreach ($request->quantity as $key => $value) {
            IngredientsToRecipes::where('id', '=', $ing_ids[$key]['id'])->update(['quantity' => $value, 'dimension' => $request->dimension[$key]]);
        }
    }
}
