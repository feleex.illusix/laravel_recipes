<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IngredientsToRecipes extends Model
{
	protected $table = 'ingredients_to_recipes';
    protected $fillable = ['ingredient_id', 'quantity', 'dimension', 'recipe_id'];
	public $timestamps = false;
}
