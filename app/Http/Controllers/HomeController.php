<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $title = 'Главная страница';
        return view('welcome')->with([
            'title' => $title
        ]);
    }
}
