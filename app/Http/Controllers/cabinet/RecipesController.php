<?php

namespace App\Http\Controllers\Cabinet;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recipes;
use App\Models\Ingredients;
use App\Models\Dimension;
use App\Models\IngredientsToRecipes;

class RecipesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($page = 1)
    {
        return view('cabinet.recipes.index')->with([
            'allRecipes' => Recipes::orderBy('id', 'desc')->paginate(2, ['*'], '', $page),
            'url' => 'recipes',
            'title' => 'Мои рецепты'
        ]);
    }

    public function create()
    {
        return view('cabinet.recipes.recipes')->with([
            'ingList' => Ingredients::lists('name', 'id')->all(),
            'listDimension' => Dimension::lists('name', 'id')->all(),
            'title' => 'Добавление/редактирование рецепта'
        ]);
    }

    public function store(Request $request)
    {
        $recipes = new Recipes;
        $this->validateRecipes($request);
        $recipes->setRecipes($request);
        return redirect('/cabinet/recipes')->with('status', 'Рецепт успешно добавлен.');
    }

    public function edit($id)
    {
        $oneRecipe = Recipes::findOrFail($id);
        return view('cabinet.recipes.recipes')->with([
            'id' => $id,
            'recipeName' => $oneRecipe->name,
            'ingList' => Ingredients::lists('name', 'id')->all(),
            'ingredientsList' => Recipes::find($id)->getRecipeIngredients->toArray(),
            'listDimension' => Dimension::lists('name', 'id')->all(),
            'recipeDimension' => Recipes::find($id)->getDimension->toArray(),
            'description' => $oneRecipe->description,
            'quantityIng' => IngredientsToRecipes::where('recipe_id', '=', $id)->get()->toArray(),
            'title' => 'Редактирование рецепта'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validateRecipes($request);
        IngredientsToRecipes::where('recipe_id', '=', $id)->delete();
        $recipes = new Recipes;
        $recipes->updateRecipes($id, $request);
        return redirect()->back()->with('status', 'Рецепт успешно обновлен.');
    }

    public function destroy($id)
    {
        Recipes::findOrFail($id)->delete();
        return redirect('/cabinet/recipes')->with('status', 'Рецепт успешно удален.');
    }

    // отображение страницы конкретного рецепта
    public function oneRecipe($id, Request $request)
    {
        if ($request['quantity']) {
            $this->validate($request, ['quantity.*' => 'required|numeric']);
            Recipes::setQuantity($id, $request);
            return redirect()->back();
        }
        return view('cabinet.recipes.one_recipe')->with([
            'id' => $id,
            'oneRecipe' => Recipes::findOrFail($id),
            'ingList' => Recipes::find($id)->getRecipeIngredients->toArray(),
            'quantity' => IngredientsToRecipes::where('recipe_id', '=', $id)->get()->toArray(),
            'listDimension' => Dimension::lists('name', 'id')->all(),
            'recipeDimension' => Recipes::find($id)->getDimension->toArray(),
            'title' => 'Просмотр рецепта'
        ]);
    }

    public function validateRecipes($request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'quantity.*' => 'required|numeric',
            'description' => 'required|max:1000'
        ]);
    }

    public function ajaxAddIng(Request $request)
    {
        if ($request->ajax()) {
            return view('cabinet.recipes.addingredients')->with([
                'ingList' => Ingredients::lists('name', 'id')->all(),
                'listDimension' => Dimension::lists('name', 'id')->all()
            ]);
        }
    }
}
