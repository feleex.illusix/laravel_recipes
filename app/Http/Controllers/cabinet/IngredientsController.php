<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Http\Controllers\Controller;
use App\Models\Ingredients;

class IngredientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($page = 1)
    {
        return view('cabinet.ingredients.index')->with([
            'allIngredients' => Ingredients::orderBy('id', 'desc')->paginate(3, ['*'], '', $page),
            'url' => 'ingredients',
            'title' => 'Ингредиенты'
        ]);
    }

    public function create()
    {
        return view('cabinet.ingredients.addingredient')->with([
            'ingredientsName' => '',
            'title' => 'Добавление ингредиента'
        ]);
    }

    public function store(Request $request)
    {
        $this->validateIng($request);
        $ingredients = new Ingredients;
        if (empty($ingredients->where('name', $request->name)->first())) {
            $ingredients->create($request->all());
            return redirect('/cabinet/ingredients')->with('status', 'Ингредиент успешно добавлен.');
        } else {
            return redirect('/cabinet/ingredients')->with('status', 'Такой ингредиент уже существует.');
        }
    }

    public function edit($id)
    {
        $ing = Ingredients::findOrFail($id);
        return view('cabinet.ingredients.addingredient')->with([
            'id' => $id,
            'ingredientsName' => $ing->name,
            'title' => 'Редактирование ингредиента'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validateIng($request);
        $ingredients = new Ingredients;
        $ingredients->findOrFail($id)->fill($request->all())->save();
        return redirect('/cabinet/ingredients')->with('status', 'Ингредиент успешно обновлен.');
    }

    public function destroy($id)
    {
        Ingredients::findOrFail($id)->delete();
        return redirect('/cabinet/ingredients')->with('status', 'Ингредиент успешно удален.');
    }

    public function validateIng($request)
    {
        $this->validate($request, ['name' => 'required|max:50']);
    }
}
