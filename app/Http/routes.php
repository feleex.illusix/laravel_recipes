<?php

Route::get('/', 'HomeController@index');

Route::auth();

Route::get('/cabinet', 'Cabinet\RecipesController@index');

Route::get('/cabinet/ingredients/{page?}', 'Cabinet\IngredientsController@index');

Route::get('/cabinet/ingredients', 'Cabinet\IngredientsController@index');

Route::get('/cabinet/addingredient', 'Cabinet\IngredientsController@create');

Route::post('/cabinet/addingredient', 'Cabinet\IngredientsController@store');

Route::get('/cabinet/ingredient/id/{id}', 'Cabinet\IngredientsController@edit');

Route::post('/cabinet/ingredient/id/{id}', 'Cabinet\IngredientsController@update');

Route::get('/cabinet/ingredients/del/{id}', 'Cabinet\IngredientsController@destroy');

Route::get('/cabinet/recipes/{page?}', 'Cabinet\RecipesController@index');

Route::get('/cabinet/recipes/id/{id}', 'Cabinet\RecipesController@edit');

Route::post('/cabinet/recipes/id/{id}', 'Cabinet\RecipesController@update');

Route::get('/cabinet/addrecipes', 'Cabinet\RecipesController@create');

Route::post('/cabinet/addrecipes', 'Cabinet\RecipesController@store');

Route::match(['get', 'post'], '/cabinet/recipe/{id}', 'Cabinet\RecipesController@oneRecipe');

Route::get('/cabinet/recipe/del/{id}', 'Cabinet\RecipesController@destroy');

Route::post('/cabinet/add_ingredients', 'Cabinet\IngredientsController@store');

Route::get('/cabinet/adding', 'Cabinet\RecipesController@ajaxAddIng');