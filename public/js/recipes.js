document.getElementById('add').onclick = function() {
    $.ajax({
        type: "get",
        url: "/cabinet/adding",
        success: function (html) {
            $('#ingredients').append(html);
        }
    });
}

document.getElementById('new_ing').onclick = function() {
    $('#add_ing').toggle();
    $(document).mouseup(function (e) {
        var div = $('#add_ing');
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            div.hide();
        }
    });
}

function deleteBlock(el) {
    $(el).parent().remove();
    return false;
}
